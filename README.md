# codeberg-hq

Workadventu.re page for Codeberg.

The map can be played at

<https://play.workadventu.re/_/global/raw.githubusercontent.com/jklippel/codeberg-hq/gh-pages/main.json>

Unfortunately workadventu.re relies on github workflows.

That's why there is a github repository for this map at:

<https://github.com/jklippel/codeberg-hq/>

## Tile-Sets

The cert-tilesets are from <https://github.com/c3CERT/rc3_tiles.git>.
See LICENSE.md for individual licenses.

## Changes

The map can be changed with the [Tiled Editor](https://www.mapeditor.org/).

## Further Reading

- <https://howto.rc3.world/2021/maps.html>
- <https://workadventu.re/map-building/>
